﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MailAttack.Models;
using MailAttack.Views;
using MailAttack.Presenters;
using MailAttack.Repository;
using MailAttack.Forms;
using System.Windows.Forms;

namespace MailAttack.Presenters
{
    public class AccountManagerPresenter
    {
        private readonly SQLiteDatabase _db;
        private readonly IAccountManagerView _view;

        public AccountManagerPresenter(SQLiteDatabase db, IAccountManagerView view)
        {
            _db = db;
            _view = view;

            _view.Presenter = this;

            UpdateAccountList();
        }

        public void UpdateAccountList()
        {
            AccountRepository repository = new AccountRepository(_db.Connection);

            _view.Accounts = repository.All();
        }

        internal void DeleteAccount(Account account)
        {
            AccountRepository repository = new AccountRepository(_db.Connection);
            repository.Remove(account);
            UpdateAccountList();
        }

        internal void EditAccount(Account account)
        {
            

            AccountForm _view = new AccountForm();
            AccountPresenter _presenter = new AccountPresenter(_db, _view, account);

            DialogResult dg = _view.ShowDialog();

            if (dg == DialogResult.Yes)
                UpdateAccountList();
        }

        internal void NewAccount()
        {
            AccountForm _view = new AccountForm();
            AccountPresenter _presenter = new AccountPresenter(_db, _view);

            DialogResult dg = _view.ShowDialog();

            if (dg == DialogResult.Yes)
                UpdateAccountList();
        }
    }
}