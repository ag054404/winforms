﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MailAttack.Models;
using MailAttack.Views;
using MailAttack.Presenters;
using MailAttack.Repository;
using MailAttack.Forms;
using System.Windows.Forms;

using MailAttack;
using MailKit.Net.Smtp;
using MimeKit;

namespace MailAttack.Presenters
{
    public class AccountPresenter
    {
        private readonly SQLiteDatabase _db;
        private readonly IAccountView _view;
        private readonly Account _account;

        public AccountPresenter(SQLiteDatabase db, IAccountView view)
        {
            _db = db;
            _view = view;
            _account = new Account();

            _view.Presenter = this;
            _view.Account = _account;

        }

        internal void SaveAccount(Account account)
        {
            AccountRepository repository = new AccountRepository(_db.Connection);

            if (account.AccountId > 0)
                repository.Update(account);
            else
                repository.Add(account);
        }

        public AccountPresenter(SQLiteDatabase db, IAccountView view, Account account)
        {
            _db = db;
            _view = view;
            _account = new Account(account);

            _view.Presenter = this;
            _view.Account = _account;
        }

        internal bool CheckAccount(Account account, string password)
        {
            MimeMessage message = new MimeMessage();
            message.From.Add(new MailboxAddress(account.Name, account.Email));
            message.To.Add(new MailboxAddress(account.Name, account.Email));
            message.Subject = $"Wiadomość testowa";
            message.Body = new TextPart("plain")
            {
                Text = "To jest wiadomość testowa wysłana za pomocą programu MailAttack"
            };
             
            using (SmtpClient client = new SmtpClient())
            {
                try
                {
                    switch(account.SecureOption)
                    {
                        case 0: client.Connect(account.Host, account.Port, false); break;
                        case 1: client.Connect(account.Host, account.Port, MailKit.Security.SecureSocketOptions.Auto); break;
                        case 2: client.Connect(account.Host, account.Port, MailKit.Security.SecureSocketOptions.SslOnConnect); break;
                        case 3: client.Connect(account.Host, account.Port, MailKit.Security.SecureSocketOptions.StartTls); break;
                        case 4: client.Connect(account.Host, account.Port, MailKit.Security.SecureSocketOptions.StartTlsWhenAvailable); break;
                        default: client.Connect(account.Host, account.Port, false); break;
                    }
                    client.Timeout = 10 * 1000;
                    client.Authenticate(account.Login, password);
                    client.Send(message);
                }
                catch(Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    client.Disconnect(true);
                }
              
            }
            return true; 
        }
    }
}
