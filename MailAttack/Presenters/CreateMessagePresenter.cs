﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MailAttack.Models;
using MailAttack.Views;
using MailAttack.Presenters;
using MailAttack.Repository;
using MailAttack.Forms;
using System.Windows.Forms;
using System.IO;

namespace MailAttack.Presenters
{
    public class CreateMessagePresenter
    {
        private readonly SQLiteDatabase _db;
        private readonly ICreateMessageView _view;
       

        public CreateMessagePresenter(SQLiteDatabase db, ICreateMessageView view)
        {
            _db = db;
            _view = view;

            _view.Presenter = this;
        }

        public void SetGroups()
        {
            GroupRepository repository = new GroupRepository(_db.Connection);

            _view.Groups = repository.All();
        }

        internal void SetAccount()
        {
            AccountRepository repository = new AccountRepository(_db.Connection);

            _view.Accounts = repository.All();
        }

        internal void BeginSendMessages()
        {
            var password = this._view.Password;
            var account = this._view.SelectedAccount;
            var group = this._view.SelectedGroup;
            var title = this._view.Topic;
            var body = this._view.Body;
            String htmlBody = String.Empty;

            if (!String.IsNullOrEmpty(this._view.BodyFromHtmlFile))
                htmlBody = File.ReadAllText(this._view.BodyFromHtmlFile);

            RecipientRepository repository = new RecipientRepository(_db.Connection);
            MessageRepository messageRepository = new MessageRepository(_db.Connection);

            var recipient = repository.AllFromGroup(group);
            var allowedRecipient = recipient.Where(r => r.Allowed == true).ToList();

            SendingProcessForm _view = new SendingProcessForm();
            SendingProcessPresenter _presenter = new SendingProcessPresenter(_view, account, password, allowedRecipient, title, body, htmlBody);

            DialogResult dg = _view.ShowDialog();

            Models.Message newMessage = new Models.Message()
            { 
                Account = account, 
                Date = DateTime.Now, 
                Group = group, 
                Title = title 
            };

            messageRepository.Add(newMessage);

            this._view.Close();

        }
    }
}
