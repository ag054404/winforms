﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MailAttack.Models;
using MailAttack.Views;
using MailAttack.Presenters;
using MailAttack.Repository;
using MailAttack.Forms;
using System.Data.SQLite;

namespace MailAttack.Presenters
{
    public class RecipientPresenter
    {
        private readonly SQLiteDatabase _db;
        private readonly IRecipientView _view;
        private readonly Recipient _recipient;

        public RecipientPresenter(SQLiteDatabase db, IRecipientView view)
        {
            _db = db;
            _view = view;
            _recipient = new Recipient();

            _view.Presenter = this;
            _view.Recipient = _recipient;

            SetAvailableGroups();
        }

        private void SetAvailableGroups()
        {
            GroupRepository repository = new GroupRepository(_db.Connection);
            _view.Groups = repository.All();
        }

        public RecipientPresenter(SQLiteDatabase db, IRecipientView view, Recipient recipient)
        {
            _db = db;
            _view = view;
            _recipient = new Recipient(recipient);

            _view.Presenter = this;
            _view.Recipient = _recipient;

            SetAvailableGroups();
        }

        internal void SaveRecipient(Recipient recipient)
        {
            RecipientRepository repository = new RecipientRepository(_db.Connection);

            if (recipient.RecipientId > 0)
                repository.Update(recipient);
            else
                repository.Add(recipient);
        }

        
    }
}
