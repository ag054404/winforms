﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MailAttack.Models;
using MailAttack.Views;
using MailAttack.Presenters;
using MailAttack.Repository;
using MailAttack.Forms;
using System.Windows.Forms;

namespace MailAttack.Presenters
{
    public class AddressBookManagerPresenter
    {
        private readonly SQLiteDatabase _db;
        private readonly IAddressBookManagerView _view;

        public AddressBookManagerPresenter(SQLiteDatabase db, IAddressBookManagerView view)
        {
            _db = db;
            _view = view;

            _view.Presenter = this;
            UpdateGroupList();
        }

        public void UpdateGroupList()
        {
            GroupRepository repository = new GroupRepository(_db.Connection);

            _view.Groups = repository.All();
        }

        internal void UpdateRecipientsList(Group group)
        {
            RecipientRepository repository = new RecipientRepository(_db.Connection);

            _view.Recipients = repository.AllFromGroup(group);
        }

        internal void UpdateRecipientsList()
        {
            RecipientRepository repository = new RecipientRepository(_db.Connection);

            _view.Recipients = repository.All();
        }

        internal void NewGroup()
        {
            GroupForm _view = new GroupForm();
            GroupPresenter _presenter = new GroupPresenter(_db, _view);

            DialogResult dg = _view.ShowDialog();

            if (dg == DialogResult.Yes)
                UpdateGroupList();
        }

        internal void EditGroup(Group group)
        {
            GroupForm _view = new GroupForm();
            GroupPresenter _presenter = new GroupPresenter(_db, _view, group);

            DialogResult dg = _view.ShowDialog();

            if (dg == DialogResult.Yes)
                UpdateGroupList();
        }

        internal void DeleteGroup(Group group)
        {
            GroupRepository repository = new GroupRepository(_db.Connection);
            repository.Remove(group);
            UpdateGroupList();
        }

        internal void DeleteRecipient(Recipient recipient)
        {
            RecipientRepository repository = new RecipientRepository(_db.Connection);
            repository.Remove(recipient);
            UpdateRecipientsList();
        }

        internal void NewRecipient()
        {
            RecipientForm _view = new RecipientForm();
            RecipientPresenter _presenter = new RecipientPresenter(_db, _view);

            DialogResult dg = _view.ShowDialog();

            if (dg == DialogResult.Yes)
                UpdateRecipientsList();
        }

        internal void EditRecipient(Recipient recipient)
        {
            RecipientForm _view = new RecipientForm();
            RecipientPresenter _presenter = new RecipientPresenter(_db, _view, recipient);

            DialogResult dg = _view.ShowDialog();

            if (dg == DialogResult.Yes)
                UpdateRecipientsList();
        }

        internal void ChangeAllowed(Recipient recipient)
        {
            recipient.Allowed = (recipient.Allowed) ? false : true;
            RecipientRepository repository = new RecipientRepository(_db.Connection);

            repository.Update(recipient);
            UpdateRecipientsList();
        }
    }
}
