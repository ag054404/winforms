﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MailAttack.Models;
using MailAttack.Views;
using MailAttack.Presenters;
using MailAttack.Repository;
using MailAttack.Forms;
using System.Windows.Forms;
using System.Threading;
using System.ComponentModel;
using MimeKit;
using MailKit.Net.Smtp;

namespace MailAttack.Presenters
{
    public class SendingProcessPresenter
    {
        private readonly ISendingProcessView _view;
        private readonly Account _account;
        private readonly String _password;
        private readonly List<Recipient> _recipients;
        private readonly MimeMessage message;
        private readonly String _topic;
        private readonly String _body;
        private readonly String _htmlBody;
        private readonly BodyBuilder _bodyBuilder;
        private readonly BackgroundWorker _bgWorker;
        public SendingProcessPresenter(ISendingProcessView view, Account account, String password, List<Recipient> recipients, String topic, String body, String htmlBody )
        {
            _view = view;
            _view.Presenter = this;
            _account = account;
            _password = password;
            _recipients = recipients;
            _topic = topic;
            _bodyBuilder = new BodyBuilder();
            _body = body;
            _htmlBody = htmlBody;

            message = new MimeMessage();
            message.From.Add(new MailboxAddress(account.Name, account.Email));
            message.Subject = _topic;

            if (String.IsNullOrEmpty(_htmlBody))
                _bodyBuilder.TextBody = _body;
            else
                _bodyBuilder.HtmlBody = _htmlBody;
            message.Body = _bodyBuilder.ToMessageBody();

            _bgWorker = new BackgroundWorker();
            _bgWorker.WorkerReportsProgress = true;
            _bgWorker.ProgressChanged += _bgWorker_ProgressChanged;
            _bgWorker.DoWork += _bgWorker_DoWork;
            _bgWorker.RunWorkerCompleted += _bgWorker_RunWorkerCompleted;
        }

        private void _bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            _view.Close();
        }

        private void _bgWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
    
            _view.PercentageProgress = e.ProgressPercentage;
            _view.CurrentSending = e.UserState.ToString();
        }

        private void _bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            int i = 0;
            int t = _recipients.Count();

            foreach(Recipient r in _recipients)
            {
                i++;
                int p = i * 100 / t;

                string c = r.Email;
                message.To.Clear();
                message.To.Add(new MailboxAddress(r.Name, r.Email));

                using (SmtpClient client = new SmtpClient())
                {
                    try
                    {
                        switch (_account.SecureOption)
                        {
                            case 0: client.Connect(_account.Host, _account.Port, false); break;
                            case 1: client.Connect(_account.Host, _account.Port, MailKit.Security.SecureSocketOptions.Auto); break;
                            case 2: client.Connect(_account.Host, _account.Port, MailKit.Security.SecureSocketOptions.SslOnConnect); break;
                            case 3: client.Connect(_account.Host, _account.Port, MailKit.Security.SecureSocketOptions.StartTls); break;
                            case 4: client.Connect(_account.Host, _account.Port, MailKit.Security.SecureSocketOptions.StartTlsWhenAvailable); break;
                            default: client.Connect(_account.Host, _account.Port, false); break;
                        }
                        client.Timeout = 10 * 1000;
                        client.Authenticate(_account.Login, _password);
                        client.Send(message);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                    finally
                    {
                        client.Disconnect(true);
                    }

                }

                _bgWorker.ReportProgress(p, c);
            }
        }

        public void StartProcess()
        {
            _bgWorker.RunWorkerAsync();
        }
    }
}
