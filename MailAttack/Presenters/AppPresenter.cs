﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MailAttack.Models;
using MailAttack.Views;
using MailAttack.Presenters;
using MailAttack.Forms;
using System.Windows.Forms;
using MailAttack.Repository;

namespace MailAttack.Presenters
{
    public class AppPresenter
    {
        private SQLiteDatabase _db;
        private readonly IAppView _view;

        public AppPresenter(IAppView view)
        {
            //_db = db;
            _view = view;

            _view.Presenter = this;
  
        }

        public void UpdateMessageList()
        {
            MessageRepository repository = new MessageRepository(_db.Connection);
            var t = repository.All().OrderByDescending(m => m.Date).ToList();
            _view.Messages = t;
        }

        internal void OpenAccountManager()
        {
            AccountManager _view = new AccountManager();
            AccountManagerPresenter _presenter = new AccountManagerPresenter(_db, _view);

            _view.ShowDialog();
        }

        internal void OpenDatabasetManager()
        {
            DatabaseManagerForm _view = new DatabaseManagerForm();
            DatabaseManagerPresenter _presenter = new DatabaseManagerPresenter(_view);

            DialogResult dg = _view.ShowDialog();

            if (dg == DialogResult.Yes)
            {     
                _db = new SQLiteDatabase(_view.FilePath);
                this._view.ActivateApplication();
                UpdateMessageList();
            }
                
            else
                this._view.InactivateApplication();
        }

        internal void OpenAddressBookManager()
        {
            AddressBookManagerForm _view = new AddressBookManagerForm();
            AddressBookManagerPresenter _presenter = new AddressBookManagerPresenter(_db, _view);

            _view.ShowDialog();
        }

        internal void OpenCreateMessageForm()
        {
            CreateMessageForm _view = new CreateMessageForm();
            CreateMessagePresenter _presenter = new CreateMessagePresenter(_db, _view);

            DialogResult dg = _view.ShowDialog();

           
            UpdateMessageList();
            
        }
    }
}
