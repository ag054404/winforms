﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MailAttack.Models;
using MailAttack.Views;
using MailAttack.Presenters;
using MailAttack.Repository;
using MailAttack.Forms;
using System.Data.SQLite;

namespace MailAttack.Presenters
{
    public class DatabaseManagerPresenter
    {
        private readonly IDatabaseManagerView _view;

        public DatabaseManagerPresenter(IDatabaseManagerView view)
        {
           
            _view = view;
           
            _view.Presenter = this;
        }

        internal void CreateNewDatabase(string path)
        {
            try
            {
                SQLiteDatabase.CreateDatabase(System.IO.Path.Combine(path));
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        
    }
}
