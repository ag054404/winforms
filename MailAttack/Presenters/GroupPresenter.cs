﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MailAttack.Models;
using MailAttack.Views;
using MailAttack.Presenters;
using MailAttack.Repository;
using MailAttack.Forms;

namespace MailAttack.Presenters
{
    public class GroupPresenter
    {
        private readonly SQLiteDatabase _db;
        private readonly IGroupView _view;
        private readonly Group _group;

        public GroupPresenter(SQLiteDatabase db, IGroupView view)
        {
            _db = db;
            _view = view;
            _group = new Group();

            _view.Presenter = this;
            _view.Group = _group;

        }

        public GroupPresenter(SQLiteDatabase db, IGroupView view, Group group)
        {
            _db = db;
            _view = view;
            _group = new Group(group);

            _view.Presenter = this;
            _view.Group = _group;
        }

        internal void SaveGroup(Group group)
        {
            GroupRepository repository = new GroupRepository(_db.Connection);

            if (group.GroupId > 0)
                repository.Update(group);
            else
                repository.Add(group);
        }

        
    }
}
