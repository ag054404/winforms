﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SQLite;
using Dapper;
using MailAttack.Models;

namespace MailAttack.Repository
{
    class RecipientRepository : Repository<Recipient>
    {
        public RecipientRepository(SQLiteConnection context) : base(context) { }
        public override void Add(Recipient item)
        {
            string sql = "INSERT INTO Recipients (Name, Email, Allowed) Values (@Name, @Email, @Allowed);";
            string sql3 = "INSERT INTO GroupRecipient(GroupId, RecipientId) Values (@GroupId, @RecipientId);";

            try
            {
                Context.Open();
                Context.Execute(sql, new { Name = item.Name, Email = item.Email, Allowed = item.Allowed });            
                Context.Execute(sql3, item.Groups.Select(g => new { GroupId = g.GroupId, RecipientId = Context.LastInsertRowId }));
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                Context.Close();
            }
        }

        public override List<Recipient> All()
        {
            string sql = "SELECT * FROM Recipients";
            string sql2 = "SELECT Groups.* FROM GroupRecipient LEFT JOIN Groups ON Groups.GroupId = GroupRecipient.GroupId WHERE GroupRecipient.RecipientId = @RecipientId";
            try
            {
                Context.Open();
                List<Recipient> collection = Context.Query<Recipient>(sql).ToList();

                collection.ForEach(r => r.Groups = Context.Query<Group>(sql2, new { RecipientId = r.RecipientId }).ToList() );

                return collection;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                Context.Close();
            }
        }

        internal List<Recipient>AllFromGroup(Group group)
        {
            string sql = "SELECT Recipients.* FROM GroupRecipient LEFT JOIN Recipients ON Recipients.RecipientId = GroupRecipient.RecipientId WHERE GroupRecipient.GroupId = @GroupId";
            string sql2 = "SELECT Groups.* FROM GroupRecipient LEFT JOIN Groups ON Groups.GroupId = GroupRecipient.GroupId WHERE GroupRecipient.RecipientId = @RecipientId";
            try
            {
                Context.Open();
                //return Context.Query<Recipient>(sql, new { GroupId = group.GroupId });
                List<Recipient> collection = Context.Query<Recipient>(sql, new { GroupId = group.GroupId }).ToList();

                collection.ForEach(r => r.Groups = Context.Query<Group>(sql2, new { RecipientId = r.RecipientId }).ToList());

                return collection;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                Context.Close();
            }
        }

        public override Recipient GetById(int Id)
        {
            string sql = "SELECT * FROM Recipients WHERE RecipinetId = @RecipientId";
            try
            {
                Context.Open();
                return Context.QueryFirstOrDefault<Recipient>(sql, new { RecipientId = Id });
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                Context.Close();
            }
        }

        public override void Remove(Recipient item)
        {
            string sql = "DELETE FROM Recipients WHERE RecipientId = @RecipientId";
            try
            {
                Context.Open();
                Context.Execute(sql, new { RecipientId = item.RecipientId });               
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                Context.Close();
            }
        }

        public override void Update(Recipient item)
        {
            string sql = "UPDATE Recipients SET Name = @Name, Email = @Email, Allowed = @Allowed WHERE RecipientId = @RecipientId";
            string sql2 = "DELETE FROM GroupRecipient WHERE RecipientId = @RecipientId";
            string sql3 = "INSERT INTO GroupRecipient(GroupId, RecipientId) Values (@GroupId, @RecipientId);";
            try
            {
                Context.Open();
                Context.Execute(sql, new { RecipientId = item.RecipientId, Name = item.Name, Email = item.Email, Allowed = item.Allowed });
                Context.Execute(sql2, new { RecipientId = item.RecipientId });
                Context.Execute(sql3, item.Groups.Select(g => new { GroupId = g.GroupId, RecipientId = item.RecipientId }));
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                Context.Close();
            }
        }
    }
}
