﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SQLite;
using Dapper;
using MailAttack.Models;

namespace MailAttack.Repository
{
    class MessageRepository : Repository<Message>
    {
        public MessageRepository(SQLiteConnection context) : base(context) { }
        public override void Add(Message item)
        {
            string sql = "INSERT INTO Messages (SmtpAccountId, Title, Date, GroupId) Values (@SmtpAccountId, @Title, @Date, @GroupId);";

            try
            {
                Context.Open();
                Context.Execute(sql, new { SmtpAccountId = item.Account.AccountId, GroupId = item.Group.GroupId, Title = item.Title, Date = item.Date });
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                Context.Close();
            }
        }

        public override List<Message> All()
        {
            string sql = "SELECT * FROM Messages";
            string sql2 = "SELECT * FROM SmtpAccounts WHERE SmtpAccounts.AccountId = @AccountId";
            string sql3 = "SELECT * FROM Groups WHERE Groups.GroupId = @GroupId";
            try
            {
                Context.Open();
                List<Message> collection = Context.Query<Message>(sql).ToList();

               collection.ForEach(m => m.Account = Context.QuerySingle<Account>(sql2, new { AccountId = m.SmtpAccountId }));
               collection.ForEach(m => m.Group = Context.QuerySingle<Group>(sql3, new { GroupId = m.GroupId }));

                return collection;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                Context.Close();
            }
        }

        public override Message GetById(int Id)
        {
            throw new NotImplementedException();
        }

        public override void Remove(Message item)
        {
            throw new NotImplementedException();
        }

        public override void Update(Message item)
        {
            throw new NotImplementedException();
        }
    }
}
