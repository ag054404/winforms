﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SQLite;
using Dapper;
using MailAttack.Models;

namespace MailAttack.Repository
{
    class AccountRepository: Repository<Account>
    {
        public AccountRepository(SQLiteConnection context) : base(context)
        {

        }
        public override void Add(Account item)
        {
            string sql = "INSERT INTO SmtpAccounts (Name, Sender, Email, Host, Port, SecureOption, Login) Values (@Name, @Sender, @Email, @Host, @Port, @SecureOption, @Login);";

            try
            {
                Context.Open();
                Context.Execute(sql,
                    new { Name = item.Name, Sender = item.Sender, Email = item.Email, Host = item.Host, Port = item.Port, SecureOption = item.SecureOption, Login = item.Login }
                    );
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                Context.Close();
            }
        }

        public override List<Account> All()
        {
            try
            {
                Context.Open();
                return Context.Query<Account>(@"SELECT * FROM SmtpAccounts").ToList();
            }
            catch(Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                Context.Close();
            }
        }

        public override Account GetById(int Id)
        {
            try
            {
                Context.Open();
                return Context.QueryFirstOrDefault<Account>("SELECT * FROM SmtpAccounts WHERE AccountId = @AccountId", new { AccountId = Id });
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                Context.Close();
            }
        }

        public override void Remove(Account item)
        {
            try
            {
                Context.Open();
                Context.Execute("DELETE FROM SmtpAccounts WHERE AccountId = @AccountId", new { AccountId = item.AccountId });
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                Context.Close();
            }
        }

        public override void Update(Account item)
        {
            try
            {
                Context.Open();
                Context.Execute("UPDATE SmtpAccounts SET Name = @Name, Sender = @Sender, Email = @Email, Host = @Host, Port = @Port, SecureOption = @SecureOption, Login = @Login WHERE AccountId = @AccountId;",
                    new { AccountId = item.AccountId, Name = item.Name, Sender = item.Sender, Email = item.Email, Host = item.Host, Port = item.Port, SecureOption = item.SecureOption, Login = item.Login }
                    );
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                Context.Close();
            }
        }
    }
}
