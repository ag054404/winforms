﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SQLite;
using Dapper;
using MailAttack.Models;

namespace MailAttack.Repository
{
    class GroupRepository : Repository<Group>
    {
        public GroupRepository(SQLiteConnection context) : base(context) { }
        public override void Add(Group item)
        {
            string sql = "INSERT INTO Groups (Name) Values (@Name);";

            try
            {
                Context.Open();
                Context.Execute(sql, new { Name = item.Name});
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                Context.Close();
            }
        }

        public override List<Group> All()
        {
            string sql = "SELECT * FROM Groups";
            try
            {
                Context.Open();
                return Context.Query<Group>(sql).ToList();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                Context.Close();
            }
        }

        public override Group GetById(int Id)
        {
            string sql = "SELECT * FROM Groups WHERE GroupId = @GroupId";
            try
            {
                Context.Open();
                return Context.QueryFirstOrDefault<Group>(sql, new { GroupId = Id });
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                Context.Close();
            }
        }

        public override void Remove(Group item)
        {
            string sql = "DELETE FROM Groups WHERE GroupId = @GroupID";
            try
            {
                Context.Open();
                Context.Execute(sql, new { GroupID = item.GroupId });
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                Context.Close();
            }
        }

        public override void Update(Group item)
        {
            string sql = "UPDATE Groups SET Name = @Name WHERE GroupId = @GroupId;";
            try
            {
                Context.Open();
                Context.Execute(sql, new { GroupId = item.GroupId, Name = item.Name });
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                Context.Close();
            }
        }
    }
}
