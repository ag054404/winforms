﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace MailAttack.Repository
{
    public abstract class Repository<T>
    {
        protected SQLiteConnection Context { get; private set; }

        public Repository(SQLiteConnection context)
        {
            Context = context;
        }

        public abstract T GetById(int Id);
        public abstract List<T> All();
        public abstract void Add(T item);
        public abstract void Remove(T item);
        public abstract void Update(T item);
    }
}
