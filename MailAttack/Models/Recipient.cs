﻿using System.Collections.Generic;

namespace MailAttack.Models
{
    public class Recipient
    {
        // PK
        public int RecipientId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public bool Allowed { get; set; }
        public List<Group> Groups { get; set; }

        public Recipient()
        {
            this.Groups = new List<Group>();
        }
        public Recipient(Recipient recipient)
        {
            this.RecipientId = recipient.RecipientId;
            this.Name = recipient.Name;
            this.Email = recipient.Email;
            this.Allowed = recipient.Allowed;
            this.Groups = recipient.Groups;
        }
    }
}
