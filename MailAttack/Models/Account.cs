﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailAttack.Models
{
    public class Account
    {
        public int AccountId { get; set; }
        public string Name { get; set; }
        public string Sender { get; set; }
        public string Email { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
        public int SecureOption { get; set; }
        public string Login { get; set; }

        public Account() {  }
        public Account(Account account)
        {
            this.AccountId = account.AccountId;
            this.Name = account.Name;
            this.Sender = account.Sender;
            this.Email = account.Email;
            this.Host = account.Host;
            this.Port = account.Port;
            this.SecureOption = account.SecureOption;
            this.Login = account.Login;
        }

        public override string ToString()
        {
            return $"{this.Name} {this.Sender}<{this.Email}>";
        }
    }
}
