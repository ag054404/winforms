﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace MailAttack.Models
{
    public class SQLiteDatabase
    {
        public SQLiteConnection Connection { private set; get; }

        public SQLiteDatabase(string filePath)
        {
            StringBuilder connectionString = new StringBuilder();

            connectionString.Append("Data Source=");
            connectionString.Append(filePath);
            connectionString.Append(";Version=3;");

            Connection = new SQLiteConnection(connectionString.ToString());
        }

        public static void CreateDatabase(string path)
        {
            SQLiteConnection.CreateFile(System.IO.Path.Combine(path));

            using (SQLiteConnection connection = new SQLiteConnection("Data Source=" + path + ";Version=3;"))
            {
                connection.Open();
                SQLiteCommand command = new SQLiteCommand(MailAttack.Properties.Resources.NewDatabase_sql, connection);
                command.ExecuteNonQuery();
            }
        }
    }
}