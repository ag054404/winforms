﻿using System;
using System.Collections.Generic;

namespace MailAttack.Models
{
    public class Group
    {
        public int GroupId { get; set; }
        public string Name { get; set; }

        public Group() { }

        public Group(Group group)
        {
            this.GroupId = group.GroupId;
            this.Name = group.Name;
        }
        public override string ToString()
        {
            return $"{this.Name}";
        }

        public override int GetHashCode()
        {
            return this.GroupId;
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            Group objAsGroup = obj as Group;
            if (objAsGroup == null) return false;
            else return this.GroupId.Equals(objAsGroup.GroupId);
        }
    }
}
