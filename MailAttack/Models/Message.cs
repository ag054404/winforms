﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailAttack.Models
{
    public class Message
    {
        // PK
        public int MessageId { get; set; }
        public string Title { get; set; }
        public DateTime Date { get; set; }

        // FK
        public int SmtpAccountId { get; set; }
        public int GroupId { get; set; }
        public Group Group { get; set; }
        public Account Account { get; set; }
    }
}
