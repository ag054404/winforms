﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using MailAttack.Forms;
using MailAttack.Presenters;

namespace MailAttack
{
    static class Program
    {
        /// <summary>
        /// Główny punkt wejścia dla aplikacji.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            AppForm _view = new AppForm();
            AppPresenter _presenter = new AppPresenter(_view);
            

            Application.Run(_view);
        }
    }
}
