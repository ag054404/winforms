﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MailAttack.Models;
using MailAttack.Presenters;
using MailAttack.Views;

namespace MailAttack.Forms
{
    public partial class CreateMessageForm : Form, ICreateMessageView
    {
        public CreateMessagePresenter Presenter { private get; set; }
        public IEnumerable<Group> Groups {
            set
            {
                value.ToList().ForEach(i => cbGroup.Items.Add(i));
            }
        }
        public Group SelectedGroup
        {
            get => cbGroup.SelectedItem as Group;
        }
        public IEnumerable<Account> Accounts { 
            set
            {
                value.ToList().ForEach(i => cbAccount.Items.Add(i));
            }
        }

        public Account SelectedAccount
        {
            get => cbAccount.SelectedItem as Account;
        }

        public string Topic
        {
            get => tbTopic.Text;
        }

        public string Body
        {
            get => rtbBody.Text;
        }

        public string BodyFromHtmlFile
        {
            get
            {
                if (File.Exists(tbHtmlFilePath.Text))
                    return tbHtmlFilePath.Text;
                return String.Empty;
            }
        }

        public string Password
        {
            get => tbPassword.Text;
        }

        public CreateMessageForm()
        {
            InitializeComponent();
        }

        private void CreateMessageForm_Load(object sender, EventArgs e)
        {
            Presenter.SetGroups();
            Presenter.SetAccount();
        }

        private void BtnSend_Click(object sender, EventArgs e)
        {
            if(!ValidFormFields())
            {
                MessageBox.Show("Nie wszystkie pola są uzupełnione!", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DialogResult result = MessageBox.Show("Czy napewno chcesz rozpocząć wysyłanie wiadomości?", "Potwiedź", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {

                try
                {
                    Presenter.BeginSendMessages();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    this.Close();
                }
            }
        }

        private bool ValidFormFields()
        {
            return cbGroup.SelectedIndex != -1 &&
                cbAccount.SelectedIndex != -1 &&
                !String.IsNullOrEmpty(tbTopic.Text.Trim()) &&
                !String.IsNullOrEmpty(rtbBody.Text.Trim());
        }

        private void btnSelectHtmlFile_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.Filter = "Plik HTML (*.html;*.htm;*.shtml;*.xhtml)|*.html;*.htm;*.shtml;*.xhtml|Wszystkie pliki (*.*)|*.*";

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    tbHtmlFilePath.Text = openFileDialog.FileName;
                }
            }
        }
    }
}
