﻿using System;
using System.Windows.Forms;

namespace MailAttack.Forms
{
    partial class AddressBookManagerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Grupy");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("Wszystkie");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddressBookManagerForm));
            this.dgvRecipients = new System.Windows.Forms.DataGridView();
            this.RecipientName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RecipientEmail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RecipientAllowed = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tvGroups = new System.Windows.Forms.TreeView();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btnNewGroup = new System.Windows.Forms.Button();
            this.btnEditGroup = new System.Windows.Forms.Button();
            this.btnDeleteGroup = new System.Windows.Forms.Button();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.btnNewRecipient = new System.Windows.Forms.Button();
            this.btnEditRecipient = new System.Windows.Forms.Button();
            this.btnDeleteRecipient = new System.Windows.Forms.Button();
            this.btnActive = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRecipients)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvRecipients
            // 
            this.dgvRecipients.AllowUserToAddRows = false;
            this.dgvRecipients.AllowUserToDeleteRows = false;
            this.dgvRecipients.AllowUserToOrderColumns = true;
            this.dgvRecipients.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvRecipients.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRecipients.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RecipientName,
            this.RecipientEmail,
            this.RecipientAllowed});
            this.dgvRecipients.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvRecipients.Location = new System.Drawing.Point(232, 143);
            this.dgvRecipients.Name = "dgvRecipients";
            this.dgvRecipients.ReadOnly = true;
            this.dgvRecipients.RowHeadersVisible = false;
            this.dgvRecipients.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvRecipients.Size = new System.Drawing.Size(529, 235);
            this.dgvRecipients.TabIndex = 1;
            this.dgvRecipients.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvRecipients_CellClick);
            this.dgvRecipients.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvRecipients_CellContentDoubleClick);
            this.dgvRecipients.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvRecipients_ColumnHeaderMouseClick);
            this.dgvRecipients.Leave += new System.EventHandler(this.dgvRecipients_Leave);
            // 
            // RecipientName
            // 
            this.RecipientName.DataPropertyName = "Name";
            this.RecipientName.FillWeight = 111.6167F;
            this.RecipientName.HeaderText = "Nazwa";
            this.RecipientName.Name = "RecipientName";
            this.RecipientName.ReadOnly = true;
            this.RecipientName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // RecipientEmail
            // 
            this.RecipientEmail.DataPropertyName = "Email";
            this.RecipientEmail.FillWeight = 111.6167F;
            this.RecipientEmail.HeaderText = "Email";
            this.RecipientEmail.Name = "RecipientEmail";
            this.RecipientEmail.ReadOnly = true;
            this.RecipientEmail.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // RecipientAllowed
            // 
            this.RecipientAllowed.DataPropertyName = "Allowed";
            this.RecipientAllowed.FillWeight = 75.92972F;
            this.RecipientAllowed.HeaderText = "Aktywny";
            this.RecipientAllowed.MinimumWidth = 50;
            this.RecipientAllowed.Name = "RecipientAllowed";
            this.RecipientAllowed.ReadOnly = true;
            this.RecipientAllowed.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.RecipientAllowed.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel1.Controls.Add(this.dgvRecipients, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.tvGroups, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 1, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(10, 10);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(764, 441);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // tvGroups
            // 
            this.tvGroups.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvGroups.Location = new System.Drawing.Point(3, 143);
            this.tvGroups.Name = "tvGroups";
            treeNode1.Name = "groups";
            treeNode1.Text = "Grupy";
            treeNode2.Checked = true;
            treeNode2.Name = "all";
            treeNode2.Text = "Wszystkie";
            this.tvGroups.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2});
            this.tvGroups.Size = new System.Drawing.Size(223, 235);
            this.tvGroups.TabIndex = 2;
            this.tvGroups.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvGroups_AfterSelect);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.btnNewGroup, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.btnEditGroup, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.btnDeleteGroup, 2, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 63);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(223, 74);
            this.tableLayoutPanel2.TabIndex = 3;
            // 
            // btnNewGroup
            // 
            this.btnNewGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNewGroup.Image = ((System.Drawing.Image)(resources.GetObject("btnNewGroup.Image")));
            this.btnNewGroup.Location = new System.Drawing.Point(3, 37);
            this.btnNewGroup.Name = "btnNewGroup";
            this.btnNewGroup.Size = new System.Drawing.Size(34, 34);
            this.btnNewGroup.TabIndex = 0;
            this.btnNewGroup.UseVisualStyleBackColor = true;
            this.btnNewGroup.Click += new System.EventHandler(this.btnNewGroup_Click);
            // 
            // btnEditGroup
            // 
            this.btnEditGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnEditGroup.Enabled = false;
            this.btnEditGroup.Image = ((System.Drawing.Image)(resources.GetObject("btnEditGroup.Image")));
            this.btnEditGroup.Location = new System.Drawing.Point(43, 37);
            this.btnEditGroup.Name = "btnEditGroup";
            this.btnEditGroup.Size = new System.Drawing.Size(34, 34);
            this.btnEditGroup.TabIndex = 1;
            this.btnEditGroup.UseVisualStyleBackColor = true;
            this.btnEditGroup.Click += new System.EventHandler(this.btnEditGroup_Click);
            // 
            // btnDeleteGroup
            // 
            this.btnDeleteGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDeleteGroup.Enabled = false;
            this.btnDeleteGroup.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteGroup.Image")));
            this.btnDeleteGroup.Location = new System.Drawing.Point(83, 37);
            this.btnDeleteGroup.Name = "btnDeleteGroup";
            this.btnDeleteGroup.Size = new System.Drawing.Size(34, 34);
            this.btnDeleteGroup.TabIndex = 2;
            this.btnDeleteGroup.UseVisualStyleBackColor = true;
            this.btnDeleteGroup.Click += new System.EventHandler(this.btnDeleteGroup_Click);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 384);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(223, 54);
            this.tableLayoutPanel3.TabIndex = 4;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 6;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel4.Controls.Add(this.btnNewRecipient, 3, 0);
            this.tableLayoutPanel4.Controls.Add(this.btnEditRecipient, 4, 0);
            this.tableLayoutPanel4.Controls.Add(this.btnDeleteRecipient, 5, 0);
            this.tableLayoutPanel4.Controls.Add(this.btnActive, 4, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(232, 63);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(529, 74);
            this.tableLayoutPanel4.TabIndex = 5;
            // 
            // btnNewRecipient
            // 
            this.btnNewRecipient.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNewRecipient.Image = ((System.Drawing.Image)(resources.GetObject("btnNewRecipient.Image")));
            this.btnNewRecipient.Location = new System.Drawing.Point(412, 3);
            this.btnNewRecipient.Name = "btnNewRecipient";
            this.btnNewRecipient.Size = new System.Drawing.Size(34, 34);
            this.btnNewRecipient.TabIndex = 0;
            this.btnNewRecipient.UseVisualStyleBackColor = true;
            this.btnNewRecipient.Click += new System.EventHandler(this.btnNewRecipient_Click);
            // 
            // btnEditRecipient
            // 
            this.btnEditRecipient.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnEditRecipient.Enabled = false;
            this.btnEditRecipient.Image = ((System.Drawing.Image)(resources.GetObject("btnEditRecipient.Image")));
            this.btnEditRecipient.Location = new System.Drawing.Point(452, 3);
            this.btnEditRecipient.Name = "btnEditRecipient";
            this.btnEditRecipient.Size = new System.Drawing.Size(34, 34);
            this.btnEditRecipient.TabIndex = 1;
            this.btnEditRecipient.UseVisualStyleBackColor = true;
            this.btnEditRecipient.Click += new System.EventHandler(this.btnEditRecipient_Click);
            // 
            // btnDeleteRecipient
            // 
            this.btnDeleteRecipient.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDeleteRecipient.Enabled = false;
            this.btnDeleteRecipient.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteRecipient.Image")));
            this.btnDeleteRecipient.Location = new System.Drawing.Point(492, 3);
            this.btnDeleteRecipient.Name = "btnDeleteRecipient";
            this.btnDeleteRecipient.Size = new System.Drawing.Size(34, 34);
            this.btnDeleteRecipient.TabIndex = 2;
            this.btnDeleteRecipient.UseVisualStyleBackColor = true;
            this.btnDeleteRecipient.Click += new System.EventHandler(this.btnRemoveRecipient_Click);
            // 
            // btnActive
            // 
            this.btnActive.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnActive.Enabled = false;
            this.btnActive.Image = ((System.Drawing.Image)(resources.GetObject("btnActive.Image")));
            this.btnActive.Location = new System.Drawing.Point(452, 43);
            this.btnActive.Name = "btnActive";
            this.btnActive.Size = new System.Drawing.Size(34, 34);
            this.btnActive.TabIndex = 5;
            this.btnActive.UseVisualStyleBackColor = true;
            this.btnActive.Click += new System.EventHandler(this.btnActive_Click);
            // 
            // AddressBookManagerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 461);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(800, 500);
            this.Name = "AddressBookManagerForm";
            this.Padding = new System.Windows.Forms.Padding(10);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "AddressBookForm";
            ((System.ComponentModel.ISupportInitialize)(this.dgvRecipients)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        

        #endregion
        private System.Windows.Forms.DataGridView dgvRecipients;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TreeView tvGroups;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button btnNewGroup;
        private System.Windows.Forms.Button btnEditGroup;
        private System.Windows.Forms.Button btnDeleteGroup;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Button btnNewRecipient;
        private System.Windows.Forms.Button btnEditRecipient;
        private System.Windows.Forms.Button btnDeleteRecipient;
        private System.Windows.Forms.DataGridViewTextBoxColumn RecipientName;
        private System.Windows.Forms.DataGridViewTextBoxColumn RecipientEmail;
        private System.Windows.Forms.DataGridViewCheckBoxColumn RecipientAllowed;
        private Button btnActive;
    }
}