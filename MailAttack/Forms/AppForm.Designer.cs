﻿namespace MailAttack.Forms
{
    partial class AppForm
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AppForm));
            this.dgvMessages = new System.Windows.Forms.DataGridView();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnOpenDatabaseManager = new System.Windows.Forms.Button();
            this.btnCreateNewMessage = new System.Windows.Forms.Button();
            this.btnOpenAddressBook = new System.Windows.Forms.Button();
            this.btnOpenAccountManager = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.MessageGroup = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MessageAccount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MessageSubject = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MessageCreateData = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMessages)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvMessages
            // 
            this.dgvMessages.AllowUserToAddRows = false;
            this.dgvMessages.AllowUserToDeleteRows = false;
            this.dgvMessages.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvMessages.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgvMessages.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MessageGroup,
            this.MessageAccount,
            this.MessageSubject,
            this.MessageCreateData});
            this.dgvMessages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMessages.Enabled = false;
            this.dgvMessages.Location = new System.Drawing.Point(128, 3);
            this.dgvMessages.MultiSelect = false;
            this.dgvMessages.Name = "dgvMessages";
            this.dgvMessages.ReadOnly = true;
            this.dgvMessages.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMessages.Size = new System.Drawing.Size(833, 359);
            this.dgvMessages.TabIndex = 1;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btnOpenDatabaseManager);
            this.flowLayoutPanel1.Controls.Add(this.btnCreateNewMessage);
            this.flowLayoutPanel1.Controls.Add(this.btnOpenAddressBook);
            this.flowLayoutPanel1.Controls.Add(this.btnOpenAccountManager);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Padding = new System.Windows.Forms.Padding(22, 0, 2, 0);
            this.flowLayoutPanel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.flowLayoutPanel1.Size = new System.Drawing.Size(119, 359);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btnOpenDatabaseManager
            // 
            this.btnOpenDatabaseManager.Image = global::MailAttack.Properties.Resources.iconfinder_box_out_299067;
            this.btnOpenDatabaseManager.Location = new System.Drawing.Point(25, 3);
            this.btnOpenDatabaseManager.Name = "btnOpenDatabaseManager";
            this.btnOpenDatabaseManager.Size = new System.Drawing.Size(75, 75);
            this.btnOpenDatabaseManager.TabIndex = 0;
            this.btnOpenDatabaseManager.Text = "\r\nBaza danych\r\n";
            this.btnOpenDatabaseManager.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnOpenDatabaseManager.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnOpenDatabaseManager.UseVisualStyleBackColor = true;
            this.btnOpenDatabaseManager.Click += new System.EventHandler(this.BtnOpenDatabaseManager_Click);
            // 
            // btnCreateNewMessage
            // 
            this.btnCreateNewMessage.Enabled = false;
            this.btnCreateNewMessage.Image = global::MailAttack.Properties.Resources.iconfinder_notepad_285631;
            this.btnCreateNewMessage.Location = new System.Drawing.Point(25, 84);
            this.btnCreateNewMessage.Name = "btnCreateNewMessage";
            this.btnCreateNewMessage.Size = new System.Drawing.Size(75, 75);
            this.btnCreateNewMessage.TabIndex = 1;
            this.btnCreateNewMessage.Text = "\r\nNowa wiadomość\r\n";
            this.btnCreateNewMessage.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnCreateNewMessage.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnCreateNewMessage.UseVisualStyleBackColor = true;
            this.btnCreateNewMessage.Click += new System.EventHandler(this.btnCreateNewMessage_Click);
            // 
            // btnOpenAddressBook
            // 
            this.btnOpenAddressBook.Enabled = false;
            this.btnOpenAddressBook.Image = global::MailAttack.Properties.Resources.iconfinder_user_group_285648;
            this.btnOpenAddressBook.Location = new System.Drawing.Point(25, 165);
            this.btnOpenAddressBook.Name = "btnOpenAddressBook";
            this.btnOpenAddressBook.Size = new System.Drawing.Size(75, 75);
            this.btnOpenAddressBook.TabIndex = 2;
            this.btnOpenAddressBook.Text = "\r\nKsiążka adresowa\r\n";
            this.btnOpenAddressBook.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnOpenAddressBook.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnOpenAddressBook.UseVisualStyleBackColor = true;
            this.btnOpenAddressBook.Click += new System.EventHandler(this.BtnOpenAddressBook_Click);
            // 
            // btnOpenAccountManager
            // 
            this.btnOpenAccountManager.Enabled = false;
            this.btnOpenAccountManager.Image = ((System.Drawing.Image)(resources.GetObject("btnOpenAccountManager.Image")));
            this.btnOpenAccountManager.Location = new System.Drawing.Point(25, 246);
            this.btnOpenAccountManager.Name = "btnOpenAccountManager";
            this.btnOpenAccountManager.Size = new System.Drawing.Size(75, 75);
            this.btnOpenAccountManager.TabIndex = 3;
            this.btnOpenAccountManager.Text = "\r\nMenadżer kont\r\n";
            this.btnOpenAccountManager.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnOpenAccountManager.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnOpenAccountManager.UseVisualStyleBackColor = true;
            this.btnOpenAccountManager.Click += new System.EventHandler(this.BtnOpenAccountManager_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 125F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.dgvMessages, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(10, 10);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 82.7907F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 17.2093F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(964, 441);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // MessageGroup
            // 
            this.MessageGroup.DataPropertyName = "Group";
            this.MessageGroup.HeaderText = "Odbiorcy";
            this.MessageGroup.MinimumWidth = 100;
            this.MessageGroup.Name = "MessageGroup";
            this.MessageGroup.ReadOnly = true;
            // 
            // MessageAccount
            // 
            this.MessageAccount.DataPropertyName = "Account";
            this.MessageAccount.HeaderText = "Konto";
            this.MessageAccount.MinimumWidth = 150;
            this.MessageAccount.Name = "MessageAccount";
            this.MessageAccount.ReadOnly = true;
            // 
            // MessageSubject
            // 
            this.MessageSubject.DataPropertyName = "Title";
            this.MessageSubject.HeaderText = "Temat";
            this.MessageSubject.MinimumWidth = 250;
            this.MessageSubject.Name = "MessageSubject";
            this.MessageSubject.ReadOnly = true;
            // 
            // MessageCreateData
            // 
            this.MessageCreateData.DataPropertyName = "Date";
            this.MessageCreateData.HeaderText = "Data stworzenia";
            this.MessageCreateData.MinimumWidth = 150;
            this.MessageCreateData.Name = "MessageCreateData";
            this.MessageCreateData.ReadOnly = true;
            this.MessageCreateData.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // AppForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 461);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1000, 500);
            this.Name = "AppForm";
            this.Padding = new System.Windows.Forms.Padding(10);
            this.Text = "MailAttack v1";
            this.Load += new System.EventHandler(this.AppForm_Load);
            this.Shown += new System.EventHandler(this.AppForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMessages)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvMessages;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button btnOpenDatabaseManager;
        private System.Windows.Forms.Button btnCreateNewMessage;
        private System.Windows.Forms.Button btnOpenAddressBook;
        private System.Windows.Forms.Button btnOpenAccountManager;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.DataGridViewTextBoxColumn MessageGroup;
        private System.Windows.Forms.DataGridViewTextBoxColumn MessageAccount;
        private System.Windows.Forms.DataGridViewTextBoxColumn MessageSubject;
        private System.Windows.Forms.DataGridViewTextBoxColumn MessageCreateData;
    }
}

