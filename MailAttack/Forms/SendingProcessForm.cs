﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MailAttack.Presenters;
using MailAttack.Views;
using MailAttack.Models;

namespace MailAttack.Forms
{
    public partial class SendingProcessForm : Form, ISendingProcessView
    {
        public SendingProcessPresenter Presenter { private get; set; }

        public int PercentageProgress { set => pbProgress.Value = value; }

        public string CurrentSending { set => lblCurrentSending.Text = $"Teraz wysyłam do: {value}"; }
        public SendingProcessForm()
        {
            InitializeComponent();
        }

        private void SendingProcessForm_Shown(object sender, EventArgs e)
        {
            Presenter.StartProcess();
        }


    }
}
