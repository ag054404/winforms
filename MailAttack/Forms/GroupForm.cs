﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MailAttack.Models;
using MailAttack.Presenters;
using MailAttack.Views;

namespace MailAttack.Forms
{
    public partial class GroupForm : Form, IGroupView
    {
        public GroupPresenter Presenter { private get; set; }
        public Group Group { get; set; }

        public GroupForm()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Presenter.SaveGroup(Group);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.DialogResult = DialogResult.Yes;
                this.Close();
            }
        }

        private void GroupForm_Load(object sender, EventArgs e)
        {
            tbName.DataBindings.Add("Text", Group, "Name");
        }
    }
}
