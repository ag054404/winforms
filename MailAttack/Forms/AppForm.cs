﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MailAttack.Presenters;

using MailAttack.Views;

namespace MailAttack.Forms
{
    public partial class AppForm : Form, IAppView
    {
        public AppPresenter Presenter {private get; set;}

        public List<MailAttack.Models.Message> Messages
        {
            set
            {
                dgvMessages.AutoGenerateColumns = false;
                dgvMessages.DataSource = value;
            }
        }
        public AppForm()
        {
            InitializeComponent();
        }

        private void AppForm_Load(object sender, EventArgs e)
        {
            this.Text = $"{ProductName} v{ProductVersion}";
        }

        private void AppForm_Shown(object sender, EventArgs e)
        {
            try
            {
                Presenter.OpenDatabasetManager();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BtnOpenAccountManager_Click(object sender, EventArgs e)
        {
            try
            {
                Presenter.OpenAccountManager();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BtnOpenDatabaseManager_Click(object sender, EventArgs e)
        {
            try
            {
                Presenter.OpenDatabasetManager();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BtnOpenAddressBook_Click(object sender, EventArgs e)
        {
            try
            {
                Presenter.OpenAddressBookManager();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCreateNewMessage_Click(object sender, EventArgs e)
        {
            try
            {
                Presenter.OpenCreateMessageForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void ActivateApplication()
        {
            btnCreateNewMessage.Enabled = true;
            btnOpenAccountManager.Enabled = true;
            btnOpenAddressBook.Enabled = true;
            dgvMessages.Enabled = true;
        }

        public void InactivateApplication()
        {
            btnCreateNewMessage.Enabled = false;
            btnOpenAccountManager.Enabled = false;
            btnOpenAddressBook.Enabled = false;
            dgvMessages.Enabled = false;
        }
    }
}
