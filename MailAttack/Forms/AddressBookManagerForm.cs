﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MailAttack.Views;
using MailAttack.Models;
using MailAttack.Presenters;
using MailAttack.Repository;

namespace MailAttack.Forms
{
    public partial class AddressBookManagerForm : Form, IAddressBookManagerView
    {
        public AddressBookManagerPresenter Presenter { set; get; }

        public IEnumerable<Group> Groups 
        {
            set => UpdateSourceTree(value);
        }

        public IEnumerable<Recipient> Recipients
        {
            set
            {
                dgvRecipients.AutoGenerateColumns = false;
                dgvRecipients.DataSource = value;
            }
        }
        public AddressBookManagerForm()
        {
            InitializeComponent();
        }

        private void UpdateSourceTree(IEnumerable<Group> value)
        {
            var node = tvGroups.Nodes["groups"];
            node.Nodes.Clear();

            foreach (Group group in value)
            {
                TreeNode newChildNode = new TreeNode();
                newChildNode.Text = group.Name;
                newChildNode.Tag = group;

                node.Nodes.Add(newChildNode);
            }
        }

        private void btnDetach_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void btnRemoveRecipient_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Czy napewno chcesz permanentnie usunąć wskazanego odbiorcę?", "Potwiedź", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                Recipient recipient = (Recipient)dgvRecipients.CurrentRow.DataBoundItem;

                try
                {
                    Presenter.DeleteRecipient(recipient);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnEditRecipient_Click(object sender, EventArgs e)
        {
            Recipient selectedRecipient = (Recipient)dgvRecipients.CurrentRow.DataBoundItem;
            try
            {
                Presenter.EditRecipient(selectedRecipient);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnNewRecipient_Click(object sender, EventArgs e)
        {
            try
            {
                Presenter.NewRecipient();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDeleteGroup_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Czy napewno chcesz usunąć wskazaną grupę?", "Potwiedź", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                Group selectedGroup = (Group)tvGroups.SelectedNode.Tag;

                try
                {
                    Presenter.DeleteGroup(selectedGroup);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnEditGroup_Click(object sender, EventArgs e)
        {
            Group selectedGroup = (Group)tvGroups.SelectedNode.Tag;
            try
            {
                Presenter.EditGroup(selectedGroup);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnNewGroup_Click(object sender, EventArgs e)
        {
            try
            {
                Presenter.NewGroup();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tvGroups_AfterSelect(object sender, TreeViewEventArgs e)
        {
            btnEditGroup.Enabled = false;
            btnDeleteGroup.Enabled = false;

            if (!(e.Node.Parent is null))
            {
                if (e.Node.Parent.Name == "groups")
                {
                    Presenter.UpdateRecipientsList((Group)e.Node.Tag);
                    btnEditGroup.Enabled = true;
                    btnDeleteGroup.Enabled = true;
                }
            }
            else
            {
                if (e.Node.Name == "all")
                {
                    Presenter.UpdateRecipientsList();
                }
            }
        }

        private void dgvRecipients_Leave(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
        }

        private void dgvRecipients_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void dgvRecipients_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            btnEditRecipient.Enabled = true;
            btnDeleteRecipient.Enabled = true;
            btnActive.Enabled = true;
        }

        private void btnActive_Click(object sender, EventArgs e)
        {
            Recipient selectedRecipient = (Recipient)dgvRecipients.CurrentRow.DataBoundItem;
            try
            {
                Presenter.ChangeAllowed(selectedRecipient);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvRecipients_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Recipient selectedRecipient = (Recipient)dgvRecipients.CurrentRow.DataBoundItem;
            try
            {
                Presenter.EditRecipient(selectedRecipient);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
