﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MailAttack.Presenters;
using MailAttack.Views;
using MailAttack.Models;

namespace MailAttack.Forms
{
    public partial class AccountForm : Form, IAccountView
    {
        public AccountPresenter Presenter { private get; set; }
        public Account Account { get; set; }
        public AccountForm()
        {
            InitializeComponent();
            EndTest();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Presenter.SaveAccount(Account);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.DialogResult = DialogResult.Yes;
                this.Close();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
        private void AccountForm_Load(object sender, EventArgs e)
        {
            tbName.DataBindings.Add("Text", Account, "Name");
            tbEmail.DataBindings.Add("Text", Account, "Email");
            tbSender.DataBindings.Add("Text", Account, "Sender");
            tbHost.DataBindings.Add("Text", Account, "Host");
            nudPort.DataBindings.Add("Value", Account, "Port");
            tbLogin.DataBindings.Add("Text", Account, "Login");
            cbSecureOptions.DataBindings.Add("SelectedIndex", Account, "SecureOption");
        }

        private void btnCheck_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Ten test wyśle do wiadomość testową na wskazane konto. Czy chcesz kontynuować?", "Potwiedź", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                BeginTest();
                backgroundWorker.RunWorkerAsync();
            }  
        }

        private void BeginTest()
        {
            tbName.Enabled = false;
            btnSave.Enabled = false;
            btnClose.Enabled = false;
            tabControl1.Enabled = false;
            progressBar1.Style = ProgressBarStyle.Marquee;
            progressBar1.Enabled = true;
            btnCheck.Enabled = false;
            cbSecureOptions.Enabled = false;
        }

        private void EndTest()
        {
            tbName.Enabled = true;
            btnSave.Enabled = true;
            btnClose.Enabled = true;
            tabControl1.Enabled = true;
            progressBar1.Style = ProgressBarStyle.Blocks;
            progressBar1.Enabled = false;
            btnCheck.Enabled = true;
            cbSecureOptions.Enabled = true;
        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                if (Presenter.CheckAccount(Account , tbPassword.Text))
                {
                    MessageBox.Show("Wysłano wiadomość. Sprawdź swoją skrzynkę odbiorczą", "Test", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            EndTest();
        }

        private void AccountForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (backgroundWorker.IsBusy)
                e.Cancel = true;
        }
    }
}
