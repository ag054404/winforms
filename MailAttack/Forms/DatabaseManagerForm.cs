﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

using MailAttack.Views;
using MailAttack.Presenters;

namespace MailAttack.Forms
{
    public partial class DatabaseManagerForm : Form, IDatabaseManagerView
    {
        public DatabaseManagerPresenter Presenter { private get; set; }
        public string FilePath
        {
            get => tbPath.Text;
            set => tbPath.Text = value;
        }
        

        public DatabaseManagerForm()
        {
            InitializeComponent();
        }

        private void BtnSelectFile_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.Filter = "Plik bazy danych (*.db)|*.db|Wszystkie pliki (*.*)|*.*";

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    tbPath.Text = openFileDialog.FileName;
                }
            }
        }

        private void BtnCreateDatabase_Click(object sender, EventArgs e)
        {
            using (SaveFileDialog saveFileDialog = new SaveFileDialog())
            {
                saveFileDialog.Filter = "Plik bazy danych (*.db)|*.db|Wszystkie pliki (*.*)|*.*";

                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        Presenter.CreateNewDatabase(saveFileDialog.FileName);
                        MessageBox.Show("Baza danych została utworzona", "Nowa baza danych", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        tbPath.Text = saveFileDialog.FileName;
                    }
                    catch (Exception e1)
                    {
                        MessageBox.Show(e1.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                }
            }
        }

        private void BtnOpenDatabase_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

        private void TbPath_TextChanged(object sender, EventArgs e)
        {
            if (File.Exists(tbPath.Text))
                btnOpenDatabase.Enabled = true;
            else
                btnOpenDatabase.Enabled = false;
        }
    }
}
