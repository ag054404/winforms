﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MailAttack.Models;
using MailAttack.Presenters;
using MailAttack.Views;

namespace MailAttack.Forms
{
    public partial class RecipientForm : Form, IRecipientView
    {
        public RecipientPresenter Presenter { private get; set; }
        public Recipient Recipient { get; set; }
        public IEnumerable<Group> Groups { set => UpdateGroups(value); }

        private void UpdateGroups(IEnumerable<Group> value)
        {
            foreach(var item in value)
            {
                if (Recipient.Groups.Where(g => g.GroupId == item.GroupId).FirstOrDefault() is null)
                    cblAvailableGroup.Items.Add(item, false);
                else
                    cblAvailableGroup.Items.Add(item, true);
            }
        }

        public RecipientForm()
        {
            InitializeComponent();
        }

        private void RecipientForm_Load(object sender, EventArgs e)
        {
            tbName.DataBindings.Add("Text", Recipient, "Name");
            tbEmail.DataBindings.Add("Text", Recipient, "Email");
            cbAllowed.DataBindings.Add("Checked", Recipient, "Allowed");
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
 
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            //IEnumerable<Group> t = cblAvailableGroup.CheckedItems.Cast<Group>();
            try
            {
                Presenter.SaveRecipient(Recipient);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.DialogResult = DialogResult.Yes;
                this.Close();
            }
        }

        private void cblAvailableGroup_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            Group clicketGroup = cblAvailableGroup.Items[e.Index] as Group;
            switch (e.NewValue)
            {
                case CheckState.Checked:
                    {
                        Recipient.Groups.Add(clicketGroup);
                    };
                    break;
                case CheckState.Unchecked:
                    {
                        Recipient.Groups.Remove(clicketGroup);
                    };
                    break;
                default:
                    break;
            }
        }

        private void RecipientForm_Shown(object sender, EventArgs e)
        {
            this.cblAvailableGroup.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.cblAvailableGroup_ItemCheck);
        }
    }
}
