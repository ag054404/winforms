﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MailAttack.Views;
using MailAttack.Presenters;
using MailAttack.Models;

namespace MailAttack.Forms
{
    public partial class AccountManager : Form, IAccountManagerView
    {
        public AccountManagerPresenter Presenter { private get; set; }

        public IEnumerable<Account> Accounts
        {
            set
            {
                dgvAccounts.AutoGenerateColumns = false;
                dgvAccounts.DataSource = value;
            }
        }
        public AccountManager()
        {
            InitializeComponent();
        }

        private void AccountManager_Load(object sender, EventArgs e)
        {

        }

        private void BtnNew_Click(object sender, EventArgs e)
        {
            try
            {
                Presenter.NewAccount();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BtnEdit_Click(object sender, EventArgs e)
        {
            Account selectedAccount = (Account)dgvAccounts.CurrentRow.DataBoundItem;
            try
            {
                Presenter.EditAccount(selectedAccount);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Czy napewno chcesz usunąć wskazane konto?", "Potwiedź", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                Account selectedAccount = (Account)dgvAccounts.CurrentRow.DataBoundItem;

                try
                {
                    Presenter.DeleteAccount(selectedAccount);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void dgvAccounts_SelectionChanged(object sender, EventArgs e)
        {
            btnEdit.Enabled = true;
            btnDelete.Enabled = true;
        }

        private void dgvAccounts_DoubleClick(object sender, EventArgs e)
        {
            Account selectedAccount = (Account)dgvAccounts.CurrentRow.DataBoundItem;
            try
            {
                Presenter.EditAccount(selectedAccount);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
