﻿using System.Collections.Generic;
using MailAttack.Presenters;
using MailAttack.Models;

namespace MailAttack.Views
{
    public interface IAccountManagerView
    {
        AccountManagerPresenter Presenter { set; }
        IEnumerable<Account> Accounts { set; }
    }
}
