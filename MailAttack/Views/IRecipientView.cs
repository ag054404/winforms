﻿using System.Collections.Generic;
using MailAttack.Presenters;
using MailAttack.Models;

namespace MailAttack.Views
{
    public interface IRecipientView
    {
        RecipientPresenter Presenter { set; }
        Recipient Recipient { get; set; }
        IEnumerable<Group> Groups {set;}
    }
}
