﻿using System.Collections.Generic;
using MailAttack.Presenters;
using MailAttack.Models;
using System;

namespace MailAttack.Views
{
    public interface ICreateMessageView
    {
        CreateMessagePresenter Presenter { set; }
        IEnumerable<Group> Groups { set; }
        Group SelectedGroup { get; }
        IEnumerable<Account> Accounts { set; }
        Account SelectedAccount { get; }
        String Topic { get; }
        String Body { get; }

        String BodyFromHtmlFile { get; }
        String Password { get; }

        void Close();
    }
}
