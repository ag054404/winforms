﻿using System.Collections.Generic;
using MailAttack.Presenters;
using MailAttack.Models;

namespace MailAttack.Views
{
    public interface ISendingProcessView
    {
        SendingProcessPresenter Presenter { set; }
        int PercentageProgress { set; }
        string CurrentSending { set; }

        void Close();
    }
}
