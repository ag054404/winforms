﻿using System.Collections.Generic;
using MailAttack.Presenters;
using MailAttack.Models;

namespace MailAttack.Views
{
    public interface IAppView
    {
        AppPresenter Presenter { set; }
        List<Message> Messages { set; }
        void ActivateApplication();
        void InactivateApplication();
    }
}
