﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MailAttack.Presenters;
using MailAttack.Models;

namespace MailAttack.Views
{
    public interface IAddressBookManagerView
    {
        AddressBookManagerPresenter Presenter { set; }

        IEnumerable<Group> Groups { set; }
        IEnumerable<Recipient> Recipients { set; }
    }
}
