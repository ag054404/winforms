﻿using System.Collections.Generic;
using MailAttack.Presenters;
using MailAttack.Models;

namespace MailAttack.Views
{
    public interface IDatabaseManagerView
    {
        DatabaseManagerPresenter Presenter { set; }
        string FilePath { get; set; }
    }
}
