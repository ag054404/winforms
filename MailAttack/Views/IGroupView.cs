﻿using System.Collections.Generic;
using MailAttack.Presenters;
using MailAttack.Models;

namespace MailAttack.Views
{
    public interface IGroupView
    {
        GroupPresenter Presenter { set; }
        Group Group { get; set; }
    }
}
