﻿using System.Collections.Generic;
using MailAttack.Presenters;
using MailAttack.Models;

namespace MailAttack.Views
{
    public interface IAccountView
    {
        AccountPresenter Presenter { set; }
        Account Account { get; set; }
    }
}
